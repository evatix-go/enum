package creationtests

import (
	"gitlab.com/evatix-go/core/coreinterface/enuminf"
	"gitlab.com/evatix-go/core/enums/stringcompareas"
	"gitlab.com/evatix-go/core/issetter"
	"gitlab.com/evatix-go/core/reqtype"
	"gitlab.com/evatix-go/enum/accesstype"
	"gitlab.com/evatix-go/enum/brackets"
	"gitlab.com/evatix-go/enum/cmdenumtypes/compresscmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/configcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/crontabscmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/decompresscmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/dnscmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/dockercmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/downloadcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/envpathcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/envvarscmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/ethernetcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/fail2bancmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/firewallcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/ftpcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/hostingplancmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/macrocmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/operatingsystemcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/packagecmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/rootcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/servicescmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/snapshotcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/sshcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/sslcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/toolingcmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/usercmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/userrolecmdnames"
	"gitlab.com/evatix-go/enum/cmdenumtypes/webservercmdnames"
	"gitlab.com/evatix-go/enum/completionstate"
	"gitlab.com/evatix-go/enum/configfilestate"
	"gitlab.com/evatix-go/enum/conntrackstate"
	"gitlab.com/evatix-go/enum/dbaction"
	"gitlab.com/evatix-go/enum/dbdrivertype"
	"gitlab.com/evatix-go/enum/dbexposetype"
	"gitlab.com/evatix-go/enum/dbuserprivillegetype"
	"gitlab.com/evatix-go/enum/eventtype"
	"gitlab.com/evatix-go/enum/fail2banbackendtype"
	"gitlab.com/evatix-go/enum/firewallprotocoltype"
	"gitlab.com/evatix-go/enum/instructiontype"
	"gitlab.com/evatix-go/enum/inttype"
	"gitlab.com/evatix-go/enum/iptablesaction"
	"gitlab.com/evatix-go/enum/iptype"
	"gitlab.com/evatix-go/enum/leveltype"
	"gitlab.com/evatix-go/enum/licensetype"
	"gitlab.com/evatix-go/enum/linescomparetype"
	"gitlab.com/evatix-go/enum/linuxservicestate"
	"gitlab.com/evatix-go/enum/linuxtype"
	"gitlab.com/evatix-go/enum/linuxvendortype"
	"gitlab.com/evatix-go/enum/logtype"
	"gitlab.com/evatix-go/enum/nginxlogtype"
	"gitlab.com/evatix-go/enum/onofftype"
	"gitlab.com/evatix-go/enum/osarchs"
	"gitlab.com/evatix-go/enum/osgroupexecution"
	"gitlab.com/evatix-go/enum/osmixtype"
	"gitlab.com/evatix-go/enum/overwritetype"
	"gitlab.com/evatix-go/enum/packageinstallmethod"
	"gitlab.com/evatix-go/enum/pathpatterntype"
	"gitlab.com/evatix-go/enum/protocoltype"
	"gitlab.com/evatix-go/enum/querymethodtype"
	"gitlab.com/evatix-go/enum/quotes"
	"gitlab.com/evatix-go/enum/resauthtype"
	"gitlab.com/evatix-go/enum/revokereason"
	"gitlab.com/evatix-go/enum/runtype"
	"gitlab.com/evatix-go/enum/scripttype"
	"gitlab.com/evatix-go/enum/servicestate"
	"gitlab.com/evatix-go/enum/sitesyncstate"
	"gitlab.com/evatix-go/enum/sqliteconnpathtype"
	"gitlab.com/evatix-go/enum/sqljointype"
	"gitlab.com/evatix-go/enum/strtype"
	"gitlab.com/evatix-go/enum/taskcategory"
	"gitlab.com/evatix-go/enum/taskpriority"
	"gitlab.com/evatix-go/enum/timeunit"
	"gitlab.com/evatix-go/enum/verifiertriggertype"
)

var simpleEnumCollectionTestCases = []enuminf.SimpleEnumer{
	issetter.Uninitialized,
	reqtype.Invalid,
	stringcompareas.Invalid.AsBasicEnumContractsBinder(),
	accesstype.Invalid,
	brackets.Invalid,

	completionstate.Invalid,

	configfilestate.Invalid,
	conntrackstate.Invalid,

	dbaction.Invalid,
	dbexposetype.Invalid,
	dbdrivertype.Invalid,

	dbuserprivilegetype.Invalid,
	eventtype.Invalid,
	fail2banbackendtype.Invalid,

	firewallprotocoltype.Invalid,
	instructiontype.Invalid,
	inttype.Invalid,
	iptype.Invalid,

	iptablesaction.Invalid.AsBasicEnumContractsBinder(),
	iptype.Invalid.AsBasicEnumContractsBinder(),

	leveltype.Invalid,
	licensetype.Invalid,
	linescomparetype.Invalid,
	linuxservicestate.Invalid,
	linuxtype.Invalid,
	linuxvendortype.Invalid,
	logtype.Invalid,

	nginxlogtype.Invalid.AsBasicEnumContractsBinder(),

	onofftype.Invalid,

	osarchs.Invalid,
	osgroupexecution.Invalid,
	osmixtype.Invalid,
	overwritetype.Invalid,

	packageinstallmethod.Invalid,
	pathpatterntype.Invalid,
	protocoltype.Invalid,

	querymethodtype.Invalid,
	quotes.Invalid,

	resauthtype.Invalid,
	revokereason.Unspecified,
	runtype.Invalid,

	scripttype.Invalid,
	servicestate.Invalid,
	sitesyncstate.Invalid,

	sqliteconnpathtype.Invalid,

	sqljointype.Invalid,
	strtype.Variant("Invalid"),
	taskcategory.Invalid,
	taskpriority.Invalid,

	timeunit.Invalid,
	verifiertriggertype.Invalid,

	compresscmdnames.Invalid.AsBasicEnumContractsBinder(),
	configcmdnames.Invalid.AsBasicEnumContractsBinder(),
	crontabscmdnames.Invalid.AsBasicEnumContractsBinder(),
	decompresscmdnames.Invalid.AsBasicEnumContractsBinder(),
	dnscmdnames.Invalid.AsBasicEnumContractsBinder(),
	dockercmdnames.Invalid.AsBasicEnumContractsBinder(),
	envpathcmdnames.Invalid.AsBasicEnumContractsBinder(),
	envvarscmdnames.Invalid.AsBasicEnumContractsBinder(),
	ethernetcmdnames.Invalid.AsBasicEnumContractsBinder(),
	downloadcmdnames.Invalid.AsBasicEnumContractsBinder(),
	ethernetcmdnames.Invalid.AsBasicEnumContractsBinder(),
	fail2bancmdnames.Invalid.AsBasicEnumContractsBinder(),
	firewallcmdnames.Invalid.AsBasicEnumContractsBinder(),
	ftpcmdnames.Invalid.AsBasicEnumContractsBinder(),
	hostingplancmdnames.Invalid.AsBasicEnumContractsBinder(),
	macrocmdnames.Invalid.AsBasicEnumContractsBinder(),
	operatingsystemcmdnames.Invalid.AsBasicEnumContractsBinder(),
	packagecmdnames.Invalid.AsBasicEnumContractsBinder(),
	rootcmdnames.Invalid.AsBasicEnumContractsBinder(),
	servicescmdnames.Invalid.AsBasicEnumContractsBinder(),
	snapshotcmdnames.Invalid.AsBasicEnumContractsBinder(),
	sshcmdnames.Invalid.AsBasicEnumContractsBinder(),
	sslcmdnames.Invalid.AsBasicEnumContractsBinder(),
	toolingcmdnames.Invalid.AsBasicEnumContractsBinder(),
	usercmdnames.Invalid.AsBasicEnumContractsBinder(),
	userrolecmdnames.Invalid.AsBasicEnumContractsBinder(),
	webservercmdnames.Invalid.AsBasicEnumContractsBinder(),
}
