<!-- ![Use Package logo](UseLogo) -->

# `enum` Intro

Package is for re-use common enums everywhere.

## Git Clone

`git clone https://gitlab.com/evatix-go/enum.git`

### 2FA enabled, for linux

`git clone https://[YourGitLabUserName]:[YourGitlabAcessTokenGenerateFromGitlabsTokens]@gitlab.com/evatix-go/enum.git`

### Prerequisites

- Update git to latest 2.29
- Update or install the latest of Go 1.15.2
- Either add your ssh key to your gitlab account
- Or, use your access token to clone it.

## Installation

`go get gitlab.com/evatix-go/enum`

### Go get issue for private package

- Update git to 2.29
- Enable go modules. (Windows : `go env -w GO111MODULE=on`, Unix : `export GO111MODULE=on`)
- Add `gitlab.com/evatix-go` to go env private

To set for Windows:

`go env -w GOPRIVATE=[AddExistingOnes;]gitlab.com/evatix-go`

To set for Unix:

`expoort GOPRIVATE=[AddExistingOnes;]gitlab.com/evatix-go`

To go get as root:

- [Linux, Go Get Fix with SSH GitLabs](https://gitlab.com/evatix-go/os-manuals/-/issues/43)

## Why enum?

Provides and reduces code lines for dealing with async stuff.

## Examples

`Code Smaples`

## Docker Examples

```shell
docker -v /on/my/host/1:/on/the/container/1 \
       -v /on/my/host/2:/on/the/container/2 \
       ...
```

## Acknowledgement

Any other packages used

## Links

* [project-layout/scripts at master · golang-standards/project-layout](https://github.com/golang-standards/project-layout/tree/master/scripts)
* Golang CrossCompile Examples
    * [Docker training details : https://t.ly/pJiQ](https://t.ly/pJiQ)
    * [CrossCompile By JetBrains: https://t.ly/JEjg](https://t.ly/JEjg)
    * [compilation - Cross compile Go on OSX? - Stack Overflow](https://stackoverflow.com/questions/12168873/cross-compile-go-on-osx)
* [gythialy/golang-cross: golang cross compiler with CGO](https://github.com/gythialy/golang-cross)
* [Mounting multiple volumes on a docker container? - Stack Overflow](https://stackoverflow.com/questions/18861834/mounting-multiple-volumes-on-a-docker-container)
* https://github.com/wille/osutil
* https://github.com/matishsiao/goInfo
* [operating system - How to reliably detect os/platform in Go - Stack Overflow](https://stackoverflow.com/questions/19847594/how-to-reliably-detect-os-platform-in-go)
* [go - How to return a default value from windows/registry with golang - Stack Overflow](https://stackoverflow.com/questions/36998532/how-to-return-a-default-value-from-windows-registry-with-golang)
* [Detect windows version in Go - Stack Overflow](https://stackoverflow.com/questions/44363911/detect-windows-version-in-go)
* [Operating System Version - Win32 apps | Microsoft Docs](https://docs.microsoft.com/en-us/windows/win32/sysinfo/operating-system-version?redirectedfrom=MSDN)
* [Makefile example](https://github.com/gythialy/golang-cross/blob/main/example/Makefile#L35-L42)

## Build of Windows

### `path_windows.go`

```go
// +build windows
package project

const PATH_SEPARATOR = '\\'
```

### `path_unix.go`

```cgo
// +build !windows
package project

const PATH_SEPARATOR = '/'
```

## Issues

- [Create your issues](https://gitlab.com/evatix-go/enum/-/issues)

## Notes

```markdown

Remaining Items

- backup
- import
- export
- osgroup

```

## Contributors

## License

[Evatix MIT License](/LICENSE)
