package accesstype

import (
	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid:        "Invalid",
		Create:         "Create",
		Update:         "Update",
		Delete:         "Delete",
		Read:           "Read",
		CreateOrUpdate: "CreateOrUpdate",
		SkipOnExist:    "SkipOnExist",
		SkipOnNonExist: "SkipOnNonExist",
		DropOnExist:    "DropOnExist",
		UpdateOnExist:  "UpdateOnExist",
	}

	onExistCheckerMap = map[Variant]bool{
		SkipOnExist:   true,
		DropOnExist:   true,
		UpdateOnExist: true,
	}

	editLogically = map[Variant]bool{
		Update:         true,
		CreateOrUpdate: true,
		UpdateOnExist:  true,
	}

	crudOnlyLogicallyMap = map[Variant]bool{
		Create: true,
		Update: true,
		Delete: true,
		Read:   true,
	}

	readEditLogicallyMap = map[Variant]bool{
		Read:           true,
		Update:         true,
		CreateOrUpdate: true,
	}

	updateOrRemoveLogicallyMap = map[Variant]bool{
		Update:         true,
		Delete:         true,
		DropOnExist:    true,
		CreateOrUpdate: true,
	}

	dropMap = map[Variant]bool{
		Delete:      true,
		DropOnExist: true,
	}

	BasicEnumImpl = enumimpl.New.BasicByte.UsingTypeSlice(
		coredynamic.TypeName(Invalid),
		Ranges[:])
)
