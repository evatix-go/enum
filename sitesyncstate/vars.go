package sitesyncstate

import (
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid:                               "Invalid",
		InitialStage:                          "InitialStage",
		Starting:                              "Starting",
		Restarting:                            "Restarting",
		Reloading:                             "Reloading",
		Stopping:                              "Stopping",
		ConfigSyntaxIssue:                     "ConfigSyntaxIssue",
		NewConfigSyntaxIssue:                  "NewConfigSyntaxIssue",
		ExistingFileModifySyntaxIssue:         "ExistingFileModifySyntaxIssue",
		Synchronizing:                         "Synchronizing",
		SyncedRunning:                         "SyncedRunning",
		SyncedSyntaxIssue:                     "SyncedSyntaxIssue",
		SyncedDuplicateDomain:                 "SyncedDuplicateDomain",
		SyncedDuplicateDefaultServer:          "SyncedDuplicateDefaultServer",
		SyncedButStopped:                      "SyncedButStopped",
		SyncedButFailedToStart:                "SyncedButFailedToStart",
		SyncedReloadRequired:                  "SyncedReloadRequired",
		SyncedRestartRequired:                 "SyncedRestartRequired",
		UnSynchronised:                        "UnSynchronised",
		UnSyncNewFileDetected:                 "UnSyncNewFileDetected",
		UnSyncExistingFileChanged:             "UnSyncExistingFileChanged",
		UnSyncExistingSiteIdentifierCorrupted: "UnSyncExistingSiteIdentifierCorrupted",
		UnSyncNewFilePlusExistingFileChanged:  "UnSyncNewFilePlusExistingFileChanged",
		UnSynchronisedRunningProperly:         "UnSynchronisedRunningProperly",
		UnSynchronisedFailedToStart:           "UnSynchronisedFailedToStart",
		UnSynchronisedFailedToStop:            "UnSynchronisedFailedToStop",
		UnSynchronisedFailedToRestart:         "UnSynchronisedFailedToRestart",
		UnSynchronisedFailedToReload:          "UnSynchronisedFailedToReload",
		UnSyncConfigRemoved:                   "UnSyncConfigRemoved",
		UnSyncDomainRemoved:                   "UnSyncDomainRemoved",
		UnSyncModuleRemoved:                   "UnSyncModuleRemoved",
		UnSyncDefaultServerDuplicate:          "UnSyncDefaultServerDuplicate",
		UnSyncDuplicateDomain:                 "UnSyncDuplicateDomain",
		UnSyncUnAuthorizedDomain:              "UnSyncUnAuthorizedDomain",
		UnSyncIncludeFileMissing:              "UnSyncIncludeFileMissing",
		UnSyncSSLFileMissing:                  "UnSyncSSLFileMissing",
		UnSyncSSLKeyMissing:                   "UnSyncSSLKeyMissing",
		UnSyncSSLCertificateMissing:           "UnSyncSSLCertificateMissing",
		RejectedSiteFilesFound:                "RejectedSiteFilesFound",
		UnSyncCache:                           "UnSyncCache",
		InvalidateCache:                       "InvalidateCache",
		RebuildSynchronizeData:                "RebuildSynchronizeData",
		RebuildCache:                          "RebuildCache",
		RebuildingCache:                       "RebuildingCache",
		RestartRequired:                       "RestartRequired",
		ReloadRequired:                        "ReloadRequired",
		StopRequired:                          "StopRequired",
	}

	DescriptionMap = map[Variant]string{
		InitialStage:                          "first time running, nothing synchronized.",
		Starting:                              "Web server starting.",
		Restarting:                            "Web server restarting.",
		Reloading:                             "Web server reloading.",
		Stopping:                              "Web server stopping.",
		ConfigSyntaxIssue:                     "Any config syntax issue found.",
		NewConfigSyntaxIssue:                  "New config add to the system has syntax issue.",
		ExistingFileModifySyntaxIssue:         "Existing file modified has syntax issue.",
		Synchronizing:                         "Synchronizing data between web server, file system, user records, database records, cache.",
		SyncedRunning:                         "Everything is synchronized and working as expected.",
		SyncedSyntaxIssue:                     "Everything is synchronized but not working as expected and some how has syntax issue.",
		SyncedDuplicateDomain:                 "Everything is synchronized but duplicate domain found, which is unexpected.",
		SyncedDuplicateDefaultServer:          "Everything is synchronized but duplicate default server found, which is unexpected.",
		SyncedButStopped:                      "Everything is synchronized but web server stopped, which is unexpected.",
		SyncedButFailedToStart:                "Everything is synchronized but failed to start the web-server, which is unexpected.",
		SyncedReloadRequired:                  "Everything is synchronized, web server reload required.",
		SyncedRestartRequired:                 "Everything is synchronized, web server restart required.",
		UnSynchronised:                        "Generic un-synchronised (could be domain changed, file state changed, banned user, ...), which is unexpected.",
		UnSyncNewFileDetected:                 "UnSynchronised new file detected, which is unexpected. Should be changed from web-ui.",
		UnSyncExistingFileChanged:             "UnSynchronised existing file detected, which is unexpected. Should be changed from web-ui.",
		UnSyncExistingSiteIdentifierCorrupted: "UnSynchronised existing domain or site corrupted it's identifier, which is unexpected. Should NOT be modified by anyone.",
		UnSyncNewFilePlusExistingFileChanged:  "UnSynchronised new file and existing file, which is unexpected. Should be changed from web-ui.",
		UnSynchronisedRunningProperly:         "UnSynchronised which is unexpected, should be changed from web-ui. However, web server is running fine temporarily.",
		UnSynchronisedFailedToStart:           "UnSynchronised which is unexpected, should be changed from web-ui. Moreover, web server is failed to start.",
		UnSynchronisedFailedToStop:            "UnSynchronised which is unexpected, should be changed from web-ui. Moreover, web server is failed to stopped.",
		UnSynchronisedFailedToRestart:         "UnSynchronised which is unexpected, should be changed from web-ui. Moreover, web server is failed to restart.",
		UnSynchronisedFailedToReload:          "UnSynchronised which is unexpected, should be changed from web-ui. Moreover, web server is failed to reload.",
		UnSyncConfigRemoved:                   "UnSynchronised config file removed, which is unexpected. Should be changed from web-ui.",
		UnSyncDomainRemoved:                   "UnSynchronised domain file removed, which is unexpected. Should be changed from web-ui.",
		UnSyncModuleRemoved:                   "UnSynchronised module file removed, which is unexpected. Should be changed from web-ui.",
		UnSyncDefaultServerDuplicate:          "UnSynchronised duplicate default server found, which is unexpected. Should be changed from web-ui.",
		UnSyncDuplicateDomain:                 "UnSynchronised duplicate domain server found, which is unexpected. Should be changed from web-ui.",
		UnSyncUnAuthorizedDomain:              "UnSynchronised unauthorized domain assign to some user, which is unexpected. Should be changed from web-ui.",
		UnSyncIncludeFileMissing:              "UnSynchronised include file missing or not found or permission issue, which is unexpected. Should be changed from web-ui.",
		UnSyncSSLFileMissing: "UnSynchronised ssl file (key or certificate or both) missing or " +
			"not found or permission issue, which is unexpected. Should be changed from web-ui.",
		UnSyncSSLKeyMissing: "UnSynchronised ssl key missing or " +
			"not found or permission issue, which is unexpected. Should be changed from web-ui.",
		UnSyncSSLCertificateMissing: "UnSynchronised ssl certificate missing or " +
			"not found or permission issue, which is unexpected. Should be changed from web-ui.",
		RejectedSiteFilesFound: "Rejected site files found, which is unexpected.",
		UnSyncCache:            "Cache is in un-synced state, which is unexpected..",
		InvalidateCache:        "Cache asked to be invalidated",
		RebuildSynchronizeData: "Asked to rebuild synchronization data",
		RebuildCache:           "Rebuild cache state",
		RebuildingCache:        "Rebuilding cache state",
		RestartRequired:        "Restart required",
		ReloadRequired:         "Reload required",
		StopRequired:           "Stop required",
	}

	ErrorStateMap = map[Variant]bool{
		ConfigSyntaxIssue:                     true,
		NewConfigSyntaxIssue:                  true,
		ExistingFileModifySyntaxIssue:         true,
		SyncedSyntaxIssue:                     true,
		SyncedDuplicateDomain:                 true,
		SyncedDuplicateDefaultServer:          true,
		SyncedButFailedToStart:                true,
		UnSynchronised:                        true,
		UnSyncNewFileDetected:                 true,
		UnSyncExistingFileChanged:             true,
		UnSyncExistingSiteIdentifierCorrupted: true,
		UnSyncNewFilePlusExistingFileChanged:  true,
		UnSynchronisedRunningProperly:         true,
		UnSynchronisedFailedToStart:           true,
		UnSynchronisedFailedToStop:            true,
		UnSynchronisedFailedToRestart:         true,
		UnSynchronisedFailedToReload:          true,
		UnSyncConfigRemoved:                   true,
		UnSyncDomainRemoved:                   true,
		UnSyncModuleRemoved:                   true,
		UnSyncDefaultServerDuplicate:          true,
		UnSyncDuplicateDomain:                 true,
		UnSyncUnAuthorizedDomain:              true,
		UnSyncIncludeFileMissing:              true,
		UnSyncSSLFileMissing:                  true,
		UnSyncSSLKeyMissing:                   true,
		UnSyncSSLCertificateMissing:           true,
		RejectedSiteFilesFound:                true,
		RebuildSynchronizeData:                true,
	}

	UnSyncStateMap = map[Variant]bool{
		UnSynchronised:                        true,
		UnSyncNewFileDetected:                 true,
		UnSyncExistingFileChanged:             true,
		UnSyncExistingSiteIdentifierCorrupted: true,
		UnSyncNewFilePlusExistingFileChanged:  true,
		UnSynchronisedRunningProperly:         true,
		UnSynchronisedFailedToStart:           true,
		UnSynchronisedFailedToStop:            true,
		UnSynchronisedFailedToRestart:         true,
		UnSynchronisedFailedToReload:          true,
		UnSyncConfigRemoved:                   true,
		UnSyncDomainRemoved:                   true,
		UnSyncModuleRemoved:                   true,
		UnSyncDefaultServerDuplicate:          true,
		UnSyncDuplicateDomain:                 true,
		UnSyncUnAuthorizedDomain:              true,
		UnSyncIncludeFileMissing:              true,
		UnSyncSSLFileMissing:                  true,
		UnSyncSSLKeyMissing:                   true,
		UnSyncSSLCertificateMissing:           true,
	}

	SyncStateMap = map[Variant]bool{
		Synchronizing:                true,
		SyncedRunning:                true,
		SyncedSyntaxIssue:            true,
		SyncedDuplicateDomain:        true,
		SyncedDuplicateDefaultServer: true,
		SyncedButStopped:             true,
		SyncedButFailedToStart:       true,
		SyncedReloadRequired:         true,
		SyncedRestartRequired:        true,
	}

	BasicEnumImpl = enumimpl.New.BasicByte.Default(
		Invalid,
		Ranges[:])
)
