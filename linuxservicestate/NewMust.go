package linuxservicestate

import "gitlab.com/evatix-go/core/errcore"

func NewMust(codeOrName string) ExitCode {
	exitCode, err := New(codeOrName)
	errcore.HandleErr(err)

	return exitCode
}
