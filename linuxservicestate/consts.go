package linuxservicestate

import (
	"gitlab.com/evatix-go/core/constants"
)

const (
	InvalidExitCode = constants.MinInt
)
