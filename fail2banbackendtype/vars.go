package fail2banbackendtype

import (
	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid:  "Invalid",
		Auto:     "auto",
		Systemd:  "systemd",
		Gamin:    "gamin",
		PyNotify: "pyinotify",
		Polling:  "polling",
	}

	capitalNames = [...]string{
		Invalid:  "Invalid",
		Auto:     "Auto",
		Systemd:  "SystemD",
		Gamin:    "GamIn",
		PyNotify: "PyiNotify",
		Polling:  "Polling",
	}

	BasicEnumImpl = enumimpl.New.BasicByte.UsingTypeSlice(
		coredynamic.TypeName(Invalid),
		Ranges[:])
)
