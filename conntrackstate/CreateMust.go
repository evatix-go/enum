package conntrackstate

import "gitlab.com/evatix-go/core/errcore"

func CreateMust(name string) Variant {
	newType, err := Create(name)
	errcore.HandleErr(err)

	return newType
}
