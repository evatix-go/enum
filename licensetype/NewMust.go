package licensetype

import "gitlab.com/evatix-go/core/errcore"

func NewMust(name string) Variant {
	exitCode, err := New(name)
	errcore.HandleErr(err)

	return exitCode
}
