package onofftype

import (
	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/errcore"
	"gitlab.com/evatix-go/core/issetter"
)

func NewUsingSetter(value issetter.Value) (Variant, error) {
	mappedItem, has := isSetterWithVariantMap[value]

	if !has {
		return Invalid, errcore.
			KeyNotExistInMapType.
			Error(
				typeConvFailedPrefixMsg+coredynamic.TypeName(value),
				mapReferenceMessage)
	}

	return Variant(mappedItem.Value()), nil
}
