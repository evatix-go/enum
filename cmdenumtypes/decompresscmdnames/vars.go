package decompresscmdnames

import (
	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid:                  "Invalid",
		Help:                     "Help",
		Install:                  "Install",
		Apply:                    "Apply",
		Compress:                 "Compress",
		Unzip:                    "Unzip",
		Extract:                  "Extract",
		Decompress:               "Decompress",
		DecompressInstall:        "DecompressInstall",
		DownloadDecompress:       "DownloadDecompress",
		DownloadDecompressRemove: "DownloadDecompressRemove",
		SneakList:                "SneakList",
		SneakSearch:              "SneakSearch",
		ListJson:                 "ListJson",
		List:                     "List",
		Histories:                "Histories",
		StateChange:              "StateChange",
		MacroHistories:           "MacroHistories",
		RemoveMacro:              "RemoveMacro",
		ExportMacro:              "ExportMacro",
		ImportMacro:              "ImportMacro",
		Backup:                   "Backup",
		Import:                   "Import",
	}

	BasicEnumImpl = enumimpl.New.BasicByte.UsingTypeSlice(
		coredynamic.TypeName(Invalid),
		Ranges[:])
)
