package inttype

import (
	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/coredata/corejson"
)

var (
	typeName = coredynamic.TypeName(Variant(-1))

	bytesToDeserializer = corejson.
				Deserialize.
				BytesTo
)
