package main

import (
	"fmt"
	"os"
	"unsafe"

	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/coredata/corejson"
	"gitlab.com/evatix-go/core/issetter"
	"gitlab.com/evatix-go/enum/brackets"
	"gitlab.com/evatix-go/enum/osmixtype"
)

func main() {
	// stringTypeTest()
	// brackets_test()

	// Windwos_Version()
	osDetailsTest()
	// serializeDeserializeTester()
	err := os.Setenv("myname", "alim")

	fmt.Println("myname, err:", err)
}

func stringTypeTest() {
	// fmt.Println(instructiontype.New("DependsOnx"))
	// fmt.Println(dbaction.New("Create"))
	//
	// alimStrType := strtype.New("alimx")
	//
	// fmt.Println(alimStrType.SafeSubString(0, 1))
	// fmt.Println(alimStrType.SafeSubString(0, alimStrType.Length()))
	// fmt.Println(alimStrType.SafeSubString(-1, alimStrType.Length()+5))
	// fmt.Println(alimStrType.SafeSubStringStart(2))
	// fmt.Println(alimStrType.SafeSubStringEnd(2))
	// fmt.Println(alimStrType.SafeSplit(2))
	// fmt.Println(alimStrType.SafeSplit(alimStrType.Length() + 5))
}

func bracketsTest() {
	bracket := brackets.Parenthesis

	fmt.Println(bracket.WrapAny("something"))

	bracket2 := brackets.ParenthesisStart
	fmt.Printf("sizeof(bracket2) = %d\n", unsafe.Sizeof(&bracket2))

	someTrue := true
	Val := issetter.True

	fmt.Printf("sizeof(someTrue) = %d\n", unsafe.Sizeof(&someTrue))
	fmt.Printf("sizeof(Val) = %d\n", unsafe.Sizeof(&Val))

	fmt.Println(bracket2.WrapAny("something2"))
	fmt.Println(bracket2.WrapFmtString("something to do with {wrapped}", "something2"))
	fmt.Println(bracket2.WrapSkipOnExist("(something2)"))
	fmt.Println(bracket2.IsWrapped("(something2)"))
}

func osDetailsTest() {
	osDetail, err := osmixtype.GetCurrentOsDetail()

	fmt.Println(osDetail.PrettyJsonString())
	fmt.Println("err", err)

	fmt.Println("current os mix types", osmixtype.CurrentOsMixType())
	fmt.Println("all os mix types", osmixtype.CurrentOsMixTypes())
	fmt.Println("all os mix map", osmixtype.CurrentOsMixTypesMap())
	fmt.Println("osmixtype.Ubuntu.IsMajorAtLeast(18) : ", osmixtype.Ubuntu.IsMajorAtLeast(18))
	fmt.Println("osmixtype.Ubuntu.IsMajorAtLeast(20) : ", osmixtype.Ubuntu.IsMajorAtLeast(20))
	fmt.Println("osmixtype.Ubuntu.IsMajorAtLeast(21) : ", osmixtype.Ubuntu.IsMajorAtLeast(21))
	fmt.Println("osmixtype.Windows.IsMajorAtLeast(10) : ", osmixtype.Windows.IsMajorAtLeast(10))
}

func serializeDeserializeTester() {
	osDetail, err := osmixtype.GetCurrentOsDetail()

	fmt.Println(err)

	slice := []interface{}{
		osDetail,
		osDetail.WindowsDetail,
	}

	for _, item := range slice {
		serializeDeserializeTesterByInput(item)
		// t := reflect.TypeOf(item)
		// for i := 0; i < t.NumField(); i++ {
		// 	fmt.Printf("%+v\n", t.Field(i))
		// 	fmt.Printf("%+v\n", t.(i))
		// }
	}

}

func serializeDeserializeTesterByInput(input interface{}) {
	json := corejson.NewPtr(input)

	if json.HasIssuesOrEmpty() {
		fmt.Println(coredynamic.TypeName(input))
		fmt.Println("Marshalling Err", json.MeaningfulErrorMessage())
	}

	finalErr := json.Deserialize(input)

	if finalErr != nil {
		fmt.Println(finalErr)
		fmt.Println(coredynamic.TypeName(input))
		fmt.Println("Json", json.PrettyJsonString())
	}
}
