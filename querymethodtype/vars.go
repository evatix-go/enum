package querymethodtype

import (
	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid:      "Invalid",
		ByFile:       "ByFile",
		ByServerName: "ByServerName",
	}

	RangesMap = map[string]Variant{
		"Invalid":      Invalid,
		"ByFile":       ByFile,
		"ByServerName": ByServerName,
	}

	BasicEnumImpl = enumimpl.New.BasicByte.UsingTypeSlice(
		coredynamic.TypeName(Invalid),
		Ranges[:])
)
