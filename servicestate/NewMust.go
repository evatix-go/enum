package servicestate

import "gitlab.com/evatix-go/core/errcore"

func NewMust(name string) Action {
	exitCode, err := New(name)
	errcore.HandleErr(err)

	return exitCode
}
