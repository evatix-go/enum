package linuxtype

var isDockerMap = map[Variant]bool{
	Docker:               true,
	DockerUbuntuServer:   true,
	DockerUbuntuServer18: true,
	DockerUbuntuServer19: true,
	DockerUbuntuServer20: true,
	DockerUbuntuServer21: true,
	DockerUbuntuServer22: true,
	DockerCentos7:        true,
	DockerCentos8:        true,
	DockerCentos9:        true,
	DockerCentos10:       true,
}
