package linuxtype

var isCentosMap = map[Variant]bool{
	Centos:       true,
	Centos7:      true,
	Centos8:      true,
	Centos9:      true,
	Centos10:     true,
	Centos11:     true,
	Centos12:     true,
	CentosStream: true,
}
