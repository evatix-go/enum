package runtype

import (
	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid:         "Invalid",
		Now:             "Now",
		OnReboot:        "OnReboot",
		OnShutdown:      "OnShutdown",
		OnEveryReboot:   "OnEveryReboot",
		OnEveryShutdown: "OnEveryShutdown",
		OnFailRetry:     "OnFailRetry",
		EveryMinute:     "EveryMinute",
		EveryHour:       "EveryHour",
		EveryDay:        "EveryDay",
		EveryMonth:      "EveryMonth",
		EveryYear:       "EveryYear",
	}

	BasicEnumImpl = enumimpl.New.BasicByte.UsingTypeSlice(
		coredynamic.TypeName(Invalid),
		Ranges[:])
)
