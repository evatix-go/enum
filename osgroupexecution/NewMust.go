package osgroupexecution

import "gitlab.com/evatix-go/core/errcore"

func NewMust(name string) Precedence {
	newType, err := New(name)
	errcore.HandleErr(err)

	return newType
}
