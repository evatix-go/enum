package osmixtype

func IsDebian() bool {
	return CurrentOsMixType().
		IsDebian()
}
