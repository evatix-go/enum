package osmixtype

func IsCurrentOsTypesContains(items ...Variant) bool {
	currentTypesMap := CurrentOsMixTypesMap()

	for _, item := range items {
		_, has := currentTypesMap[item]

		if has {
			return true
		}
	}

	return false
}
