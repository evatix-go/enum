package osmixtype

func IsUbuntu() bool {
	return CurrentOsMixType().
		IsUbuntu()
}
