package osmixtype

func IsCentOs() bool {
	return CurrentOsMixType().
		IsCentos()
}
