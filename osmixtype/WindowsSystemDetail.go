package osmixtype

import (
	"gitlab.com/evatix-go/core/coreversion"
	"gitlab.com/evatix-go/enum/inttype"
	"gitlab.com/evatix-go/enum/strtype"
)

type WindowsSystemDetail struct {
	WindowsVersion     inttype.Variant     // eg. 8, 10, 11
	ServerVersion      inttype.Variant     // eg. 2016, 2019
	CurrentVersion     coreversion.Version // eg. 6.3 https://t.ly/XLFC
	CompiledVersion    coreversion.Version
	ReleaseId          inttype.Variant
	CurrentBuildId     inttype.Variant
	BuildBranch        strtype.Variant
	InstallType        strtype.Variant
	SystemRoot         strtype.Variant
	Edition            strtype.Variant // example: "ServerStandard", "Professional", "Enterprise", "Workstation"
	CompositionEdition strtype.Variant // eg. "Enterprise" - For Windows 10, "ServerStandard" -- Windows Server
	IsServer           bool            // refers to Windows Server
	IsClient           bool            // refers to Windows 10
	CompiledError      error
}

func (it *WindowsSystemDetail) IsNull() bool {
	return it == nil
}

func (it *WindowsSystemDetail) WinVer() inttype.Variant {
	if it.IsClient {
		return it.WindowsVersion
	}

	if it.IsServer {
		return it.ServerVersion
	}

	return inttype.Zero
}

func (it *WindowsSystemDetail) IsDefined() bool {
	return it != nil
}

func (it *WindowsSystemDetail) IsNullOr(isCondition bool) bool {
	return it == nil || isCondition
}

func (it *WindowsSystemDetail) IsDefinedPlus(isCondition bool) bool {
	return it != nil && isCondition
}

func (it WindowsSystemDetail) IsWindows8() bool {
	return it.IsWindowsEqual(8)
}

func (it WindowsSystemDetail) IsWindowsGreaterEqual(number int) bool {
	if it.IsNullOr(it.IsServer) {
		return false
	}

	return it.WindowsVersion.IsGreaterEqual(number)
}

func (it *WindowsSystemDetail) IsWindowsServerGreaterEqual(number int) bool {
	if it.IsNullOr(it.IsClient) {
		return false
	}

	return it.ServerVersion.IsGreaterEqual(number)
}

func (it WindowsSystemDetail) IsWindowsEqual(number int) bool {
	if it.IsNullOr(it.IsServer) {
		return false
	}

	return it.WindowsVersion.IsEqual(number)
}

func (it *WindowsSystemDetail) IsWindowsServerEqual(number int) bool {
	if it.IsNullOr(it.IsClient) {
		return false
	}

	return it.ServerVersion.IsEqual(number)
}

func (it WindowsSystemDetail) IsWindows7() bool {
	return it.IsWindowsEqual(7)
}

func (it WindowsSystemDetail) IsWindows10() bool {
	return it.IsWindowsEqual(10)
}

// IsWindows11
//
//  https://t.ly/Jsr1,
//  https://prnt.sc/wAZ5uQScNqk_
func (it WindowsSystemDetail) IsWindows11() bool {
	return it.CurrentBuildId.IsGreaterEqual(
		windows11BuildIdentifier)
}

func (it WindowsSystemDetail) IsWindowsSever() bool {
	return it.IsDefinedPlus(it.IsServer)
}

func (it WindowsSystemDetail) IsWindowsSever2016() bool {
	return it.IsWindowsServerEqual(2016)
}

func (it WindowsSystemDetail) IsWindowsSever2019() bool {
	return it.IsWindowsServerEqual(2019)
}

func (it WindowsSystemDetail) IsWindowsSeverGreaterEqual2016() bool {
	return it.IsWindowsGreaterEqual(2016)
}

func (it WindowsSystemDetail) IsWindowsSeverGreaterEqual2019() bool {
	return it.IsWindowsServerGreaterEqual(2019)
}
