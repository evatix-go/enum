package osmixtype

import (
	"gitlab.com/evatix-go/core/codestack"
	"gitlab.com/evatix-go/core/errcore"
	"golang.org/x/sys/windows/registry"
)

func NewWindowsSystemDetailGetter() (windowsSysDetailDefiner, error) {
	k, err := registry.OpenKey(
		registry.LOCAL_MACHINE,
		windowsRegistryKeyPathForOsInfo,
		registry.QUERY_VALUE)

	if err != nil {
		return nil, errcore.FailedToParseType.CombineWithAnother(
			"registry.LOCAL_MACHINE",
			"couldn't read registry key!",
			windowsRegistryKeyPathForOsInfo).ErrorNoRefs(
			codestack.StacksStringDefault())
	}

	generator := &windowsSystemDetailGenerator{
		rawErrCollection: errcore.RawErrCollection{},
		rootRegistryKey:  k,
	}

	return generator, nil
}
