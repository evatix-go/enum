package osmixtype

func IsRedhat() bool {
	return CurrentOsMixType().
		IsRedHatEnterpriseLinux()
}
