package osmixtype

func CurrentOsMixType() Variant {
	currentOsType := currentOsMixTypeOnce.
		Value()

	return Variant(currentOsType)
}
