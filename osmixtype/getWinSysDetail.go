package osmixtype

import (
	"gitlab.com/evatix-go/core/coredata/corejson"
	"gitlab.com/evatix-go/core/errcore"
	"gitlab.com/evatix-go/core/osconsts"
	"gitlab.com/evatix-go/core/ostype"
)

func getWinSysDetail() (windowsSystemDetailGetter, error) {
	if osconsts.IsWindows {
		return NewWindowsSystemDetailGetter()
	}

	return nil, errcore.NotSupportedType.Error(
		"Not supported other than windows system",
		corejson.NewPtr(ostype.CurrentGroupVariant).JsonString())
}
