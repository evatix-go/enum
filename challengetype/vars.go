package challengetype

import (
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid:   "Invalid",
		Dns01:     "Dns01",
		Http01:    "Http01",
		TlsAlpn01: "TlsAlpn01",
	}

	BasicEnumImpl = enumimpl.New.BasicByte.Default(
		Invalid,
		Ranges[:])
)
