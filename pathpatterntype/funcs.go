package pathpatterntype

type (
	Formatter func(patternType Variant) string
)
