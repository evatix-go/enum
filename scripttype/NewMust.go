package scripttype

import "gitlab.com/evatix-go/core/errcore"

// NewMust
//
//  Creates string to the type Variant
func NewMust(name string) Variant {
	newType, err := New(name)
	errcore.HandleErr(err)

	return newType
}
