package scripttype

import "gitlab.com/evatix-go/core/osconsts"

func OsDefaultScriptType() Variant {
	if osconsts.IsWindows {
		return Powershell
	}

	return Bash
}
