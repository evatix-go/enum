package appservicestype

import (
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid:        "Invalid",
		TaskRunner:     "TaskRunner",
		ServiceChecker: "ServiceChecker",
		HealthChecker:  "HealthChecker",
		AutoUpdater:    "AutoUpdater",
		AuditChecker:   "AuditChecker",
		Synchronizer:   "Synchronizer",
		Reporter:       "Reporter",
		WebBackend:     "WebBackend",
		WebFrontend:    "WebFrontend",
		WebFrontApi:    "WebFrontApi",
		Logger:         "Logger",
		Cli:            "Cli",
	}

	aliasMap = map[string]byte{
		"task-runner":         TaskRunner.Value(),
		"tasker":              TaskRunner.Value(),
		"service-checker":     ServiceChecker.Value(),
		"health-checker":      HealthChecker.Value(),
		"auto-update-checker": AutoUpdater.Value(),
		"auto-updater":        AutoUpdater.Value(),
		"syncer":              Synchronizer.Value(),
		"synchronizer":        Synchronizer.Value(),
		"feedback":            Reporter.Value(),
		"reporter":            Reporter.Value(),
		"web-back":            WebBackend.Value(),
		"web-front":           WebFrontend.Value(),
		"web-front-api":       WebFrontApi.Value(),
		"logger":              Logger.Value(),
		"cli":                 Cli.Value(),
	}

	BasicEnumImpl = enumimpl.New.BasicByte.DefaultWithAliasMap(
		Invalid,
		Ranges[:],
		aliasMap)
)
