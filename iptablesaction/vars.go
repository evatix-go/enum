package iptablesaction

import (
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
)

var (
	Ranges = [...]string{
		Invalid: "Invalid",
		Accept:  "ACCEPT",
		Reject:  "REJECT",
		Drop:    "DROP",
		Limit:   "LIMIT",
		Return:  "RETURN",
	}

	BasicEnumImpl = enumimpl.New.BasicByte.CreateUsingSlicePlusAliasMapOptions(
		true,
		Invalid,
		Ranges[:],
		nil)
)
