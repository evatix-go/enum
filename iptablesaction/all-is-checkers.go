package iptablesaction

func Is(
	rawStr string,
	expectedVersion Variant,
) bool {
	convData, err := New(rawStr)

	if err != nil {
		return false
	}

	return convData == expectedVersion
}
